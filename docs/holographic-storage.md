# Взаимное вычисление общего дерева поиска

Дерево всех возможных списков нет необходимости пересчитывать или скачивать. Необходимо только обмениваться деревьями-заголовками последних изменений.

Все свзязи формируются в едином порядке, так как единственный способ дополнить базу - это пододвинуть общий указатель по единой цепочке хешей.

При обращении к базе для получения n-битного значения мы либо получаем результат (указатель на ранее найденный элемент, либо метод возвращает нам генератор, который мы крутим пока не найдется нужное значение. 

Несмотря на то что каждый участник сети будет искать свое значения с определенного бита - они все в конечном итоге обязаны собрать одну и ту же конструкцию - все возможные комбинации всех возможных разбиений до текущего бита.

При обмене историей узлы вычисляют взаимную дельту и накладывают на свое дерево списков отличия из чужого, в том же порядке что и передано.

Отработав создание всех недостающих элементов, обратившись к ним в порядке распположения на общей оси (вычисляя неостающие элементы в том же порядке в котором они вычислялись другим пиром)  курсор у обоих участников примет одинаковое положение.

Каждый из пиров должен только пройтись по тем элементам которых у него нет.

Если результыты сходятся - то узлы добавляют друг друга в свои базы и свою историю и регистрируют обмен данными друг с другом.

## Сжатие основной очереди операций

Каждая транзакция должна освободить столько же места сколько и займет. При добавлении записи в базу лог транзакций будет сжиматься и заменяться столько раз, сколько необходимо для освободнения места под  транзакцию.

## Правила добавления записей в общую базу

 Значения с общим числом битов хранятся в одной таблице. Значения большой разрядности с меньшим числом битов - ссылка на таблицы с соответствующим количеством бит в значении.

### Размер записи

Не более чем n\*8 бит, где n - последнее максимально заполненное n-битное целое.